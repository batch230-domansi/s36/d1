const express = require("express");
// express.Router() method allows access to HTTP methods
const router = express.Router();
const taskController = require("../controllers/taskController");

// Create - task routes
router.post("/addTask", taskController.createTaskController);

// Get all tasks  
router.get("/allTasks", taskController.getAllTasksController);

router.delete("/deleteTask/:taskId", taskController.deleteTaskController);

// Change the status of a task to "complete"
// This route expects to receive a put request at the URL "/tasks/:id/complete"
// The whole URL is at "http://localhost:3001/tasks/:id/complete"
// We cannot use put("/tasks/:id") again because it has already been used in our route to update a task
router.put("/:id/archive", (req, res) => {
	taskController.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
}) 


module.exports = router;
